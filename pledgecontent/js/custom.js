$(function () {
    var minVal = 1, maxVal = 20; // Set Max and Min values
      // Increase product quantity on cart page
        $(".increaseQty").on('click', function(){
                var $parentElm = $(this).parents(".qtySelector");
                $(this).addClass("clicked");
                setTimeout(function(){
                    $(".clicked").removeClass("clicked");
                },100);
                var value = $parentElm.find(".qtyValue").val();
                if (value < maxVal) {
                    value++;
                }
                $parentElm.find(".qtyValue").val(value);
        });
         // Decrease product quantity on cart page
        $(".decreaseQty").on('click', function(){
            var $parentElm = $(this).parents(".qtySelector");
            $(this).addClass("clicked");
            setTimeout(function(){
                $(".clicked").removeClass("clicked");
            },100);
            var value = $parentElm.find(".qtyValue").val();
            if (value > 1) {
                value--;
            }
            $parentElm.find(".qtyValue").val(value);
        });
        // Decrease product quantity on cart page
        
        //count dynamic id and appear counter box
        $('.list-of-support.slec-plane ul li').each( function(i){
            $(this).attr("id","suppo-item-" + i++);
        });
        $(".list-of-support.slec-plane ul li .item-tick").click(function(){
            var idselect = $(this).parents().attr("id");
            console.log(idselect);
            $("#"+idselect+" .std-count").toggleClass('std-count-active')
        });

        $(".list-of-support.slec-plane ul li .item-tick label").click(function(){
            var idselect = $(this).parents().parents().parents().attr("id");
            console.log(idselect);
            $("#"+idselect+" .std-count").toggleClass('std-count-active')
        });
        //count dynamic id and appear counter box
        
        // Hide Show Select Pldge btn 
        // $('.btn-top input[type="radio"]').click(function(){
        //     $(".list-of-support.slec-plane ul li:not(.list-of-support.slec-plane ul li:last-child)").addClass("active-attr"); 
        //     $(".list-of-support.slec-plane ul li:not(.list-of-support.slec-plane ul li:last-child)").removeClass("notactive-attr"); 
        //     $(".list-of-support.slec-plane ul li:last-child").addClass("notactive-attr"); 
        //     $(".list-of-support.slec-plane ul li:last-child").addClass("active-attr"); 
        // });
        // $('.btn-bottom input[type="radio"]').click(function(){
        //     $(".list-of-support.slec-plane ul li:not(.list-of-support.slec-plane ul li:last-child)").removeClass("active-attr"); 
        //     $(".list-of-support.slec-plane ul li:not(.list-of-support.slec-plane ul li:last-child)").addClass("notactive-attr"); 
        //     $(".list-of-support.slec-plane ul li:last-child").removeClass("notactive-attr"); 
        //     $(".list-of-support.slec-plane ul li:last-child").addClass("active-attr"); 
        // });
        // Hide Show Select Pldge btn 
    
        $('ul li:last-child').addClass('last-child');

        // $('.pldge-rec-ul li:eq(3) .item-tick').click(function(){
        //     // $('.recognize-form').removeClass('recognize-form-active');
        //     // $(this).addClass('recognize-form-active');

        //     $('.recognize-form').removeClass('current');
        //     $(this).addClass('current');
        // });

        $(".pldge-rec-ul li:eq(2) label, .pldge-rec-ul li:eq(2) .item-tick").click(function(){
            $(".recognize-form").toggle();
        });

        $(".list-of-support.slec-plane ul li.last-child label, .list-of-support.slec-plane ul li.last-child .item-tick").click(function(){
            $(".user-am-input").toggleClass('user-am-set');
        });
        

});

